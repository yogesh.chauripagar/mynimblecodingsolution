package com.ycventure.mynimblecodingsolution.common

/**
 * The class helps to initialize  the variable which will be constant
 * It will help to configure all the constant in single class
 * @version 1.0.0
 * @author Yogesh Chauripagar on 14-09-19.
 */

object AppConstants {
    const val TOKEN_URL = "https://nimble-survey-api.herokuapp.com/oauth/token"
    const val BASE_URL = "https://nimble-survey-api.herokuapp.com/surveys.json?access_token="
    const val TOKEN_PREFERENCES = "token_pref" //shared pref
}
