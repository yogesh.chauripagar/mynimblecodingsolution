package com.ycventure.mynimblecodingsolution.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.NavigationUI.*
import com.ycventure.mynimblecodingsolution.common.AppConstants
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


/**
 * The class will fetch the survey and display
 * @version 1.0.0
 * @author Yogesh Chauripagar on 14-09-19.
 */

class MainActivity : AppCompatActivity() {

    var TAG = this.javaClass.canonicalName
    private val surveyFragment = SurveyFragment()
    var sharedpreferences: SharedPreferences? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(com.ycventure.mynimblecodingsolution.R.layout.activity_main)

        setSupportActionBar(toolbar)

        setupNavigation()

        sharedpreferences = getSharedPreferences(AppConstants.TOKEN_PREFERENCES, Context.MODE_PRIVATE)
    }


    override fun onSupportNavigateUp(): Boolean {
        return navigateUp(findNavController(this, com.ycventure.mynimblecodingsolution.R.id.nav_host_fragment), drawer_layout)
    }

    private fun setupNavigation() {
        val navController = findNavController(this, com.ycventure.mynimblecodingsolution.R.id.nav_host_fragment)

        // Update action bar to reflect navigation
        setupActionBarWithNavController(this, navController, drawer_layout)


        // Handle nav drawer item clicks
        nav_view.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            drawer_layout.closeDrawers()
            true
        }

        // Tie nav graph to items in nav drawer
        setupWithNavController(nav_view, navController)
    }


}







