package com.ycventure.mynimblecodingsolution.view

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.ycventure.mynimblecodingsolution.model.SurveyModel
import kotlinx.android.synthetic.main.survey_item.view.*


/**
 * The class helps to bind data with the recycler view
 * @version 1.0.0
 * @author Yogesh Chauripagar on 14-09-19.
 */

class SurveyAdapter(var context: Context, var surveys: Array<SurveyModel>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var v = LayoutInflater.from(context).inflate(com.ycventure.mynimblecodingsolution.R.layout.survey_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Item).bindData(surveys!!.get(position), context)
    }

    override fun getItemCount(): Int {
        return surveys!!.size
    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(surveyModel: SurveyModel, context: Context) {

            val img = ImageView(context)
            Picasso.get()
                    .load(surveyModel.coverImageUrl + "l")
                    .into(img, object : Callback {
                        override fun onSuccess() {
                            Log.d("ImageLoading::", "success")

                            itemView.background_survey.background = img.drawable
                        }

                        override fun onError(e: Exception?) {
                            Log.d("ImageLoading::", "error")
                        }
                    })

            itemView.text_survey_name.text = surveyModel.title
            itemView.text_survey_description.text = surveyModel.description
            itemView.btn_take_survey.setOnClickListener {

                val intent = Intent(context, TakeSurveyActivity::class.java)
                startActivity(context, intent, null)

            }
        }
    }
}
