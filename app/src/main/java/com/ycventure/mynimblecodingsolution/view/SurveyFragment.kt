package com.ycventure.mynimblecodingsolution.view

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.view.*
import com.ycventure.mynimblecodingsolution.common.AppConstants
import com.ycventure.mynimblecodingsolution.model.SurveyModel
import com.ycventure.mynimblecodingsolution.model.SurveyViewModel
import com.ycventure.mynimblecodingsolution.model.TokenManager
import kotlinx.android.synthetic.main.fragment_surveys.*


/**
 * The class displays the survey
 * @version 1.0.0
 * @author Yogesh Chauripagar on 14-09-19.
 */

class SurveyFragment : Fragment() {
    lateinit var mSurveyViewModel: SurveyViewModel
    var accessToken: String? = ""
    var sharedpreferences: SharedPreferences? = null
    private val tokenManager = TokenManager()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {

        setHasOptionsMenu(true)
        val root = inflater.inflate(com.ycventure.mynimblecodingsolution.R.layout.fragment_surveys, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setUpObserver()

        sharedpreferences = context!!.getSharedPreferences(AppConstants.TOKEN_PREFERENCES, Context.MODE_PRIVATE)
        accessToken = sharedpreferences!!.getString("access_token", "")
        if (accessToken!!.trim() == "") {
            tokenManager.refreshToken(context!!)
        } else {
            this.mSurveyViewModel.fetchSurveyData(accessToken!!, context!!)
        }
        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(rv_display_surveys)

        indicator.animatePageSelected(2)

        // Observe Data Change
        rv_display_surveys.adapter?.registerAdapterDataObserver(indicator.adapterDataObserver)

        // CircleIndicator2 for RecyclerView
        indicator.attachToRecyclerView(rv_display_surveys, pagerSnapHelper)


    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        activity!!.menuInflater.inflate(com.ycventure.mynimblecodingsolution.R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        return if (id == com.ycventure.mynimblecodingsolution.R.id.action_refresh) {
            this.mSurveyViewModel.fetchSurveyData(accessToken!!,context!!)
           // this.mSurveyViewModel.fetchSurveyData("", context!!)
            true
        } else super.onOptionsItemSelected(item)

    }


    private fun setUpObserver() {

        this.mSurveyViewModel = ViewModelProviders.of(this).get(SurveyViewModel::class.java)
        val surveyObserver = Observer<Array<SurveyModel>> { surveyModelsArray ->

            // Update the UI
            rv_display_surveys.layoutManager = LinearLayoutManager(context)
            rv_display_surveys.adapter = SurveyAdapter(context!!, surveyModelsArray)
            indicator.createIndicators(surveyModelsArray!!.size, 0)
            indicator.visibility = View.VISIBLE

        }

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        this.mSurveyViewModel.allSurveys.observe(this, surveyObserver)

    }


}