package com.ycventure.mynimblecodingsolution.data
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.ycventure.mynimblecodingsolution.model.Converters
import com.ycventure.mynimblecodingsolution.model.SurveyModel


/**
 * The class will create database with table for Surveys
 * @version 1.0.0
 * @author Yogesh Chauripagar on 14-09-19.
 */

@Database(entities = arrayOf(SurveyModel::class), version = 1)
@TypeConverters((Converters::class))
abstract class SurveyDatabase : RoomDatabase() {


    abstract fun surveyDao(): SurveyDao

    companion object {
        private var DB_INSTANCE: SurveyDatabase? = null

        @JvmStatic
        fun getDatabaseInstance(context: Context): SurveyDatabase? {
            if (DB_INSTANCE == null) {
                synchronized(SurveyDatabase::class.java) {
                    if (DB_INSTANCE == null) {

                        DB_INSTANCE = Room.databaseBuilder(context.applicationContext,
                                SurveyDatabase::class.java, "surveys_database")
                                .build()
                    }
                }
            }
            return DB_INSTANCE
        }
    }
}