package com.ycventure.mynimblecodingsolution.data

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.ycventure.mynimblecodingsolution.model.SurveyModel


/**
 * The class helps to create DAO for Surveys
 * @version 1.0.0
 * @author Yogesh Chauripagar on 14-09-19.
 */

@Dao
interface SurveyDao {
    
    @Query("SELECT * FROM surveys")
    fun getAllSurveys(): LiveData<Array<SurveyModel>>

    @Insert
    fun insertAll(surveyModel: Array<SurveyModel>)

    @Delete
    fun deleteAll(surveyModel: Array<SurveyModel>)

}