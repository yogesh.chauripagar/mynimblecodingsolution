package com.ycventure.mynimblecodingsolution.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.graphics.drawable.Drawable
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable
import java.util.ArrayList

/**
 * The class helps for accessing survey data like id,name,description & background image
 * @version 1.0.0
 * @author Yogesh Chauripagar on 14-09-19.
 */

@Entity(tableName = "surveys")
data class SurveyModel(
        @SerializedName("id")
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: String = "",

        @NonNull
        @SerializedName("title")
        @ColumnInfo(name = "title")
        var title: String = "",

        @SerializedName("description")
        @NonNull
        @ColumnInfo(name = "description")
        var description: String = "",

        @SerializedName("cover_image_url")
        @NotNull
        @ColumnInfo(name = "cover_image_url")
        var coverImageUrl: String = ""
)

