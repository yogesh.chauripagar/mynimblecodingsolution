package com.ycventure.mynimblecodingsolution.model

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.SharedPreferences
import android.support.v4.app.Fragment
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.ycventure.mynimblecodingsolution.common.AppConstants
import com.ycventure.mynimblecodingsolution.view.MainActivity
import com.ycventure.mynimblecodingsolution.view.SurveyFragment
import org.jetbrains.anko.indeterminateProgressDialog
import org.json.JSONObject
import java.util.*
import android.os.Build
import com.ycventure.mynimblecodingsolution.R


class TokenManager {

    private var responseToken = ""
    var sharedpreferences: SharedPreferences? = null

    fun refreshToken(context: Context): String {

        val queue = Volley.newRequestQueue(context)

        //show progress
        val progressDialog = context!!.indeterminateProgressDialog(message = context.getString(com.ycventure.mynimblecodingsolution.R.string.refreshing_token_message))
        progressDialog.show()

        val tokenRequest = object : StringRequest(Request.Method.POST,
                AppConstants.TOKEN_URL, Listener { response ->
            Log.d("TokenManager::", "printing response::")
            try {

                //getting the whole json object from the response
                val obj = JSONObject(response)
                responseToken = obj.getString("access_token")

                Log.d("TokenManager::", "response:::responseToken::$responseToken")

                if (progressDialog.isShowing)
                    progressDialog.dismiss()

                sharedpreferences = context.getSharedPreferences(AppConstants.TOKEN_PREFERENCES, Context.MODE_PRIVATE)
                val editor = sharedpreferences!!.edit()
                editor.putString("access_token", responseToken)
                editor.apply()


                  //todo  call fetch survey

            } catch (e: Exception) {

                e.printStackTrace()

                if (progressDialog.isShowing)
                    progressDialog.dismiss()

                Toast.makeText(context, e.localizedMessage, Toast.LENGTH_LONG).show()

            }
        }, Response.ErrorListener { error ->
            Log.d("TokenManager::", "Error ::" + error.localizedMessage)
            responseToken = ""

            if (progressDialog.isShowing)
                progressDialog.dismiss()

            Toast.makeText(context, error.localizedMessage, Toast.LENGTH_LONG).show()
        }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params["grant_type"] = "password"
                // volley will escape this
                params["randomFieldFilledWithAwkwardCharacters"] = "{{%stuffToBe Escaped/"
                params["username"] = "carlos@nimbl3.com"
                params["password"] = "antikera"
                return params
            }
        }
        queue.add(tokenRequest)

        return responseToken
    }


}
