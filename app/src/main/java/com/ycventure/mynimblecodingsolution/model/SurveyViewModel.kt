package com.ycventure.mynimblecodingsolution.model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.content.Context
import android.content.SharedPreferences
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.ycventure.mynimblecodingsolution.R
import com.ycventure.mynimblecodingsolution.common.AppConstants
import com.ycventure.mynimblecodingsolution.data.SurveyDao
import com.ycventure.mynimblecodingsolution.data.SurveyDatabase
import com.ycventure.mynimblecodingsolution.view.SurveyAdapter
import kotlinx.android.synthetic.main.fragment_surveys.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.indeterminateProgressDialog


/**
 * The class helps for storing data
 * @author Yogesh Chauripagar on 16-03-19.
 * @version 1.0.0
 */

class SurveyViewModel(application: Application) : AndroidViewModel(application) {
    var TAG = this.javaClass.canonicalName

    private val mSurveyDao: SurveyDao
    private val mSurveyDatabase: SurveyDatabase?
    private val tokenManager = TokenManager()


    init {

        this.mSurveyDatabase = SurveyDatabase.getDatabaseInstance(application)
        this.mSurveyDao = this.mSurveyDatabase!!.surveyDao()
    }

    override fun onCleared() {
        super.onCleared()
    }

    var allSurveys: LiveData<Array<SurveyModel>> = mSurveyDao.getAllSurveys()

    fun insertAll(surveyModelArray: Array<SurveyModel>) {
        doAsync {
            mSurveyDao.deleteAll(surveyModelArray)
            mSurveyDao.insertAll(surveyModelArray)
        }
    }


    // function for fetching Survey Data
    fun fetchSurveyData(accessToken: String, context: Context) {


        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(context)

        //show progress
        val progressDialog = context.indeterminateProgressDialog(message = context.getString(R.string.fetching_survey_message))
        progressDialog.show()

        // Request a string response from the provided URL.
        val stringReq = StringRequest(Request.Method.GET, AppConstants.BASE_URL + accessToken,
                Response.Listener<String> { response ->

                    Log.d(TAG, "printing response::")
                    Log.d(TAG, "response:::$response")

                    val gson = Gson()
                    val surveyModelArray = gson.fromJson(response, Array<SurveyModel>::class.java)

                    try {
                        insertAll(surveyModelArray)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    val allSurveysModel = allSurveys
                    Log.d(TAG, allSurveysModel.toString())

                    if (progressDialog.isShowing)
                        progressDialog.dismiss()

                },
                Response.ErrorListener { error ->

                    val errorCode = error.networkResponse.statusCode

                    if (errorCode == 401) { //Token expired
                        Log.d(TAG, "Error ::" + error.localizedMessage)
                        val token = tokenManager.refreshToken(context)

                        Log.d(TAG, "accessToken::$token")

                        if (accessToken.trim() != "") { //first time getting token
                            fetchSurveyData(accessToken,context)
                        }

                        if (progressDialog.isShowing)
                            progressDialog.dismiss()
                    } else {

                        Toast.makeText(context, context.getString(R.string.err_fetching_survey),
                                Toast.LENGTH_LONG).show()

                        if (progressDialog.isShowing)
                            progressDialog.dismiss()
                    }

                })
        queue.add(stringReq)

    }


}
